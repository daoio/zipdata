## 十分简单的gzip压缩模块

### 安装

```javascript

npm i zipdata

```

### 使用

压缩后的数据是Buffer类型。

```javascript

const zipdata = require('zipdata');

zipdata('./file.txt')
    .then(data => {
        //do something
    }).catch(err => {
        // handle error
    });

//在async声明的函数中，直接使用await.

;(async () => {

    try {
        let data = await zipdata('./a.js');
    } catch (err) {
        //...
    }

})();


```

使用第二个参数表示是否为数据：

```javascript

//如果data是压缩的数据不是文件路径
zipdata(data, true)
    .then(zdata => {
        //...
    })
    .catch(err => {
        //...
    })

```

