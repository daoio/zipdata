'use strict'

let zipdata = require('../zipdata.js')

;(async () => {

  let orgdata = 'test for zipdata, zipdata use zlib and gzip an unzip'

  let zdata = await zipdata(orgdata, true)

  let data = await zipdata.unzip(zdata, true)

  console.log(data.toString() === orgdata, data instanceof Buffer)

})()

